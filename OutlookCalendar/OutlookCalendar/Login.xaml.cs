﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
        }

        public event EventHandler myEventLogin;
        public event EventHandler loginToCalendar;
       
        private void btnLogin_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (txtLoginID.Text == "drl" && txtPwd.Password == "1234")
            {
                try
                {
                    if (proc != null)
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                }

                myEventLogin(this, null);
            }
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            //txtLoginID.Focus();
            DateTime today= DateTime.Now.Date;
            lblTodayDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
            //today.DayOfWeek + ", " + today.Date.ToString("dd/MMM/yyyy");
        }

        Process proc;
        private void txtLoginID_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void txtPwd_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            loginToCalendar(this, null);
        }

        private void txtLoginID_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtLoginID.Text == "Login ID")
            {
                txtLoginID.Text = "";
                txtLoginID.Opacity = 1;
            }
        }

        private void txtLoginID_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtLoginID.Text))
            {
                txtLoginID.Text = "Login ID";
            }
        }

        private void txtPwd_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtPwd.Password == "password")
            {
                txtPwd.Password = "";
                txtPwd.Opacity = 1;
            }
        }

        private void txtPwd_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPwd.Password))
            {
                txtPwd.Password = "password";
            }
        }

    }
}
