﻿using OutlookCalendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for Schedule.xaml
    /// </summary>
    public partial class Schedule : UserControl
    {
        public Schedule()
        {
            InitializeComponent();
        }

        public event EventHandler createSchedule;        

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            txtEvent.Focus();
            //datePckr.TimePickerVisibility = Visibility.Hidden;
            timePckr.Value = DateTime.Now;
            timePckrEnd.Value = DateTime.Now;
            DateTime today= DateTime.Now.Date;
            lblTodayDate.Text = today.DayOfWeek + ", " + today.Date.ToString("dd/MMM/yyyy");
            

            #region
            //List<OutlookCalendar.Model.Duration> lstDurations = new OutlookCalendar.Model.Duration().fetchPreDefinedDurations();
            //cmbDuration.ItemsSource = lstDurations;
            //cmbDuration.DisplayMemberPath = "DurationName";
            #endregion
        }
      
        private void btnSave_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            DateTime dtStartDate = DateTime.Now;
            DateTime dtEndDate = DateTime.Now;
            if (timePckr.Value.HasValue)
            {
                string startDate = DateTime.Now.ToShortDateString() + "  " + timePckr.Value.Value.Hour + ":" + timePckr.Value.Value.Minute;
                dtStartDate = Convert.ToDateTime(startDate);
            }
            if (timePckrEnd.Value.HasValue)
            {
                string endDate = DateTime.Now.ToShortDateString() + "  " + timePckrEnd.Value.Value.Hour + ":" + timePckrEnd.Value.Value.Minute;
                dtEndDate = Convert.ToDateTime(endDate);
            }

            if (dtStartDate < dtEndDate && txtEvent.Text != null)
            {
                Appointment appointment = new Appointment();
                appointment.MeetingID = new Random().Next(1000, 9999);
                appointment.Subject = txtEvent.Text;
                appointment.Attendees = txtAttendees.Text;
                appointment.StartTime = dtStartDate;
                appointment.EndTime = dtEndDate;
                appointment.ShowAs = cmbAvlMeetingRoom.Text;

                #region
                //OutlookCalendar.Model.Duration selectedDuration = cmbDuration.SelectedItem as OutlookCalendar.Model.Duration;
                //appointment.EndTime = dtStartDate.AddMinutes(selectedDuration.DurationValue);
                //appointment.SelectedDuration = selectedDuration.DurationName;
                //appointment.Duration = selectedDuration.DurationValue;
                //appointment.Reminder = cmbReminder.Text;
                //appointment.Repeat = cmbRepeat.Text;
                //appointment.ShowAs = cmbshowAs.Text;
                #endregion

                try
                {
                    if (proc != null)
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                }

                createSchedule(appointment, null);
            }
        }

        Process proc;
        private void txtEvent_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void txtAttendees_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        public event EventHandler scheduleToCalendar;
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            scheduleToCalendar(this, null);
        }
    }
}
