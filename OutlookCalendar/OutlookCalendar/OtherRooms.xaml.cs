﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for OtherRooms.xaml
    /// </summary>
    public partial class OtherRooms : UserControl
    {
        public OtherRooms()
        {
            InitializeComponent();
        }

        public event EventHandler backToCalendar;
        public event EventHandler bookMeetingRoom;
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            backToCalendar(this, null);
        }

        private void scrollVwrOtherRooms_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnBookMeetingRoom_TouchDown_1(object sender, TouchEventArgs e)
        {
            bookMeetingRoom(this, null);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            lblDayMonthDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
        }
    }
}
