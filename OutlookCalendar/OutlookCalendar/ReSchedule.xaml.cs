﻿using OutlookCalendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for ReSchedule.xaml
    /// </summary>
    public partial class ReSchedule : UserControl
    {
        public Appointment SelectedAppointment { get; set; }
        public event EventHandler modifiedAppointment;
        public event EventHandler closeReSchedule;
        public event EventHandler releaseAppointment;
        
        public ReSchedule()
        {
            InitializeComponent();
        }
        
        List<Model.Duration> lstDurations;
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (SelectedAppointment != null)
            {
                this.DataContext = SelectedAppointment;

                DateTime today = DateTime.Now.Date;
                lblTodayDate.Text = today.DayOfWeek + ", " + today.Date.ToString("dd/MMM/yyyy");

                #region
                //lstDurations = new OutlookCalendar.Model.Duration().fetchPreDefinedDurations();
                //cmbDuration.ItemsSource = lstDurations;
                //cmbDuration.DisplayMemberPath = "DurationName";
                //cmbExtendDuration.ItemsSource = lstDurations;
                //cmbExtendDuration.DisplayMemberPath = "DurationName";
                //cmbDuration.SelectedItem = lstDurations.Where(c => c.DurationValue.Equals(SelectedAppointment.Duration)).FirstOrDefault();
                #endregion
            }
        }

        private void btnCloseReSchedule_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            closeReSchedule(this, null);
        }

        private void btnEdit_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            txtEvent.IsEnabled = true;
            txtAttendees.IsEnabled = true;
            timePicker.IsEnabled = true;
            timePickerEnd.IsEnabled = true;
            cmbAvlMeetingRoom.IsEnabled = true;
            startEditCancelPanel.Visibility = Visibility.Collapsed;
            stkUpdateCancelPanel.Visibility = Visibility.Visible;            
        }

        private void btnCancel_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCancelReSchedule.Visibility = Visibility.Visible;
            //closeReSchedule(this, null);
        }

        public event EventHandler cancelMeeting;
        private void btnCancelReSchedule_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCancelReSchedule.Visibility = Visibility.Collapsed;
        }

        private void btnExtend_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelExtendDuration.Visibility = Visibility.Visible;
        }

        private void btnCloseExtndDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelExtendDuration.Visibility = Visibility.Collapsed;           
        }

        private void btnSaveExtendDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelExtendDuration.Visibility = Visibility.Collapsed;
        }

        private void btnOkCancelReSchdl_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            cancelMeeting(this, null);
        }

        private void btnRelease_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCheckRelease.Visibility = Visibility.Visible;
            //releaseAppointment(SelectedAppointment, null);
        }

        private void btnUpdate_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            DateTime dtStartDate = DateTime.Now;
            DateTime dtEndDate = DateTime.Now;
            if (timePicker.Value.HasValue)
            {
                string startDate = DateTime.Now.ToShortDateString() + "  " + timePicker.Value.Value.Hour + ":" + timePicker.Value.Value.Minute;
                dtStartDate = Convert.ToDateTime(startDate);
            }
            if (timePickerEnd.Value.HasValue)
            {
                string endDate = DateTime.Now.ToShortDateString() + "  " + timePickerEnd.Value.Value.Hour + ":" + timePickerEnd.Value.Value.Minute;
                dtEndDate = Convert.ToDateTime(endDate);
            }
            if (dtStartDate < dtEndDate && txtEvent.Text != null)
            {
                try
                {
                    if (proc != null)
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                }
                //Appointment SelectedAppointment = new Appointment();
                //SelectedAppointment.Subject = txtEvent.Text;
                //SelectedAppointment.Attendees = txtAttendees.Text;
                //SelectedAppointment.StartTime = dtStartDate;
                //SelectedAppointment.EndTime = dtEndDate;
                //SelectedAppointment.ShowAs = cmbAvlMeetingRoom.Text;
                modifiedAppointment(SelectedAppointment, null);
            }
        }

        private void btnCancelUpdate_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            txtEvent.IsEnabled = false;
            txtAttendees.IsEnabled = false;
            timePicker.IsEnabled = false;
            timePickerEnd.IsEnabled = false;
            cmbAvlMeetingRoom.IsEnabled = false;
            startEditCancelPanel.Visibility = Visibility.Visible;
            stkUpdateCancelPanel.Visibility = Visibility.Collapsed;
        }

        private void btnReleaseOk_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            releaseAppointment(SelectedAppointment, null);
        }

        private void btnCheckReleaseClose_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCheckRelease.Visibility = Visibility.Collapsed;
        }

        private void btnCheckIn_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            btnRelease.IsEnabled = true;
            btnExtend.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnCancel.IsEnabled = false;
            btnCheckOut.Visibility = Visibility.Visible;
            btnCheckIn.Visibility = Visibility.Collapsed;
        }

        private void btnCheckOut_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            btnRelease.IsEnabled = false;
            btnExtend.IsEnabled = false;
            btnEdit.IsEnabled = true;
            btnCancel.IsEnabled = true;
            btnCheckOut.Visibility = Visibility.Collapsed;
            btnCheckIn.Visibility = Visibility.Visible;
        }

        Process proc;
        private void txtEvent_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void txtAttendees_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (proc != null)
                {
                    if (proc.HasExited)
                    {
                        proc = Process.Start(@"osk.exe");
                    }
                    else
                    {
                        proc.Kill();
                        proc = Process.Start(@"osk.exe");
                    }
                }
                else
                {
                    proc = Process.Start(@"osk.exe");
                }
            }
            catch (Exception ex)
            {
            }
        }

        public event EventHandler reScheduleToCalendar;
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            reScheduleToCalendar(this, null);
        }
    }
}
