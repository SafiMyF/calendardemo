﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OutlookCalendar.Model
{
    public class Appointment : BindableObject
    {
        private int meetingID;

        public int MeetingID
        {
            get { return meetingID; }
            set { meetingID = value; }
        }
        
        private string subject;
        public string Subject 
        {
            get { return subject; }
            set
            {
                if (subject != value)
                {
                    subject = value;
                    RaisePropertyChanged("Subject");
                }
            }
        }

        private string attendees;

        public string Attendees
        {
            get { return attendees; }
            set { attendees = value; }
        }
        

        private string location;
        public string Location
        {
            get { return location; }
            set
            {
                if (location != value)
                {
                    location = value;
                    RaisePropertyChanged("Location");
                }
            }
        }

        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set
            {
                if (startTime != value)
                {
                    startTime = value;
                    RaisePropertyChanged("StartTime");
                }
            }
        }

        private DateTime endTime;
        public DateTime EndTime
        {
            get { return endTime; }
            set
            {
                if (endTime != value)
                {
                    endTime = value;
                    RaisePropertyChanged("EndTime");
                }
            }
        }

        private string body;
        public string Body
        {
            get { return body; }
            set
            {
                if (body != value)
                {
                    body = value;
                    RaisePropertyChanged("Body");
                }
            }
        }

        private string selectedDuration;

        public string SelectedDuration
        {
            get { return selectedDuration; }
            set { selectedDuration = value; }
        }
        

        private int duration;

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        

        private string reminder;

        public string Reminder
        {
            get { return reminder; }
            set
            {
                reminder = value;
                RaisePropertyChanged("Reminder");
            }
        }

        private string showAs;

        public string ShowAs
        {
            get { return showAs; }
            set
            {
                showAs = value;
                RaisePropertyChanged("ShowAs");
            }
        }

        private string repeat;

        public string Repeat
        {
            get { return repeat; }
            set
            {
                repeat = value;
                RaisePropertyChanged("Repeat");
            }
        }
        
        
        public override string ToString()
        {
            return Subject;
        }
    }
}
