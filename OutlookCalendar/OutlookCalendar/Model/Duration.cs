﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookCalendar.Model
{
    public class Duration
    {
        public Duration()
        {
            DurationName = string.Empty;
            DurationValue = 0;
        }

        public string DurationName { get; set; }
        public int DurationValue { get; set; }

        public List<Duration> fetchPreDefinedDurations()
        {
            List<Duration> lstDuration = new List<Duration>();
            lstDuration.Add(new Duration() { DurationName = "0 minutes", DurationValue = 0 });
            lstDuration.Add(new Duration() { DurationName = "30 minutes", DurationValue = 30 });
            lstDuration.Add(new Duration() { DurationName = "1 Hour", DurationValue = 60 });
            lstDuration.Add(new Duration() { DurationName = "90 minutes", DurationValue = 90 });
            lstDuration.Add(new Duration() { DurationName = "2 Hours", DurationValue = 120 });
            lstDuration.Add(new Duration() { DurationName = "All Day", DurationValue = 1440 });
            lstDuration.Add(new Duration() { DurationName = "Custom", DurationValue = -1 });
            return lstDuration;
        }
    }
}
