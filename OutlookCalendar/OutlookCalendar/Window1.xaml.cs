﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OutlookCalendar.Model;
using System.Windows.Media.Animation;
using OutlookCalendar.Controls;
using System.Windows.Threading;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private Appointments appointments = new Appointments();
        int currentMeetingID;
        bool checkedIn = false;

        public Window1()
        {
            InitializeComponent();
            DataContext = appointments;
        }

        private void Calendar_AddAppointment(object sender, RoutedEventArgs e)
        {
            LoadLogin();
        }

        private void LoadLogin()
        {
            Login login = new Login();
            ParentPanel.Children.Add(login);

            login.myEventLogin += login_myEventLogin;
            login.loginToCalendar += login_loginToCalendar;            
        }

        void login_loginToCalendar(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        void login_myEventLogin(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();

            Schedule Sch = new Schedule();
            ParentPanel.Children.Add(Sch);
            Sch.createSchedule += Sch_createSchedule;
            Sch.scheduleToCalendar += Sch_scheduleToCalendar;
        }

        void Sch_scheduleToCalendar(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        void Sch_createSchedule(object sender, EventArgs e)
        {
            Appointment newAppointment = sender as Appointment;
            appointments.Add(newAppointment);
            appointmentControl.Appointments = null;
            appointmentControl.Appointments = appointments;
            LoadHomePage();
            Sch_myEventCancel(sender, e);
        }

        void Sch_myEventCancel(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        private void appointmentControl_ModifyAppointment_1(object sender, RoutedEventArgs e)
        {
            OutlookCalendar.Controls.CalendarAppointmentItem item = e.OriginalSource as OutlookCalendar.Controls.CalendarAppointmentItem;
            Appointment appointmentitem = item.Content as Appointment;          

            ParentPanel.Children.Clear();
            ReSchedule reSchedule = new ReSchedule();
            reSchedule.SelectedAppointment = appointmentitem;
            reSchedule.modifiedAppointment += reSchedule_modifiedAppointment;
            reSchedule.closeReSchedule += reSchedule_closeReSchedule;
            reSchedule.cancelMeeting += reSchedule_cancelMeeting;
            reSchedule.releaseAppointment += reSchedule_releaseAppointment;
            ParentPanel.Children.Add(reSchedule);
            reSchedule.reScheduleToCalendar += reSchedule_reScheduleToCalendar;
        }
       
        void reSchedule_reScheduleToCalendar(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        void reSchedule_releaseAppointment(object sender, EventArgs e)
        {
            Appointment newAppointment = sender as Appointment;
            appointments.Remove(newAppointment);
            appointmentControl.Appointments = null;
            appointmentControl.Appointments = appointments;
            checkedIn = false;
            ParentPanel.Children.Clear();
        }

        void reSchedule_cancelMeeting(object sender, EventArgs e)
        {
            //code for cancel meeting
            ParentPanel.Children.Clear();
        }

        void reSchedule_closeReSchedule(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        void reSchedule_modifiedAppointment(object sender, EventArgs e)
        {
            Appointment modifiedAppointment = sender as Appointment;
            appointments.Remove(appointments.Where(c => c.MeetingID.Equals(modifiedAppointment.MeetingID)).FirstOrDefault());
            appointments.Add(modifiedAppointment);
            ParentPanel.Children.Clear();
            appointmentControl.Appointments = null;
            appointmentControl.Appointments = appointments;
            //reSchedule_closeReSchedule(sender, e);
        }

        private void btnGoToCalendar_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            Storyboard LoadCalendar = TryFindResource("LoadCalendar_SB") as Storyboard;
            LoadCalendar.Begin();
            ParentPanel.Children.Clear();
            grdCalendar.Visibility = Visibility.Visible;
            grdHomePage.Visibility = Visibility.Hidden;
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            Storyboard LoadHome = TryFindResource("LoadHome_SB") as Storyboard;
            LoadHome.Begin();
            grdCalendar.Visibility = Visibility.Hidden;
            grdHomePage.Visibility = Visibility.Visible;
            LoadHomePage();       
        }

        private void btnOtherRooms_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadOtherRooms();
        }

        private void LoadOtherRooms()
        {
            ParentPanel.Children.Clear();                   //clear the parent panel childrens
            OtherRooms otrRoom = new OtherRooms();
            ParentPanel.Children.Add(otrRoom);              //add OtherRooms object to parentPanel
            //raise OtherRooms events here
            otrRoom.backToCalendar += otrRoom_backToCalendar;
            otrRoom.bookMeetingRoom += otrRoom_bookMeetingRoom;
        }

        void otrRoom_bookMeetingRoom(object sender, EventArgs e)
        {
            LoadLogin();
        }

        void otrRoom_backToCalendar(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        private void btnBookRoomFooter_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadLogin();        
        }

        private void btnBookRoomHome_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadLogin();
        }

        private void btnSearchRoom_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadOtherRooms();
        }

        private void LoadHomePage()
        {
            Appointment app = new Appointment();
            List<Appointment> lst = appointments.ToList();

            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            lblDayMonthDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");

            bool isBetween = false;
            bool isLater = false;
            bool meetingRoomEmpty = false;
            bool isLessThan15Mins = false;
            int isBetweenMeeting = 0;
            string appStartTime = string.Empty;
            int appointmentId = 0;
            foreach (var item in lst)
            {
                DateTime startTime = item.StartTime;
                DateTime endTime = item.EndTime;
                DateTime currentTime = DateTime.Now;

                if (isBetween = currentTime < endTime && currentTime > startTime)
                {
                    isBetweenMeeting++;
                    appointmentId = item.MeetingID;
                    Appointment nxtMeeting = appointments.Where(c => c.MeetingID.Equals(appointmentId)).FirstOrDefault();

                    meetingRoomEmpty = startTime.AddMinutes(2) > currentTime;
                    isLessThan15Mins = currentTime > startTime && currentTime < startTime.AddMinutes(2);

                    if (isLessThan15Mins && checkedIn == false)
                    {
                        currentMeetingID = item.MeetingID;
                        orangeBg.Visibility = Visibility.Visible;
                        redBg.Visibility = Visibility.Collapsed;
                        greenBg.Visibility = Visibility.Collapsed;
                        btnCheckIn.Visibility = Visibility.Visible;
                        btnExtend.Visibility = Visibility.Visible;
                        btnRelease.Visibility = Visibility.Visible;
                        btnEdit.Visibility = Visibility.Visible;
                        lblMeetingTill.Text = "";
                    }
                    else if(checkedIn)
                    {
                        currentMeetingID = item.MeetingID;
                        redBg.Visibility = Visibility.Visible;
                        orangeBg.Visibility = Visibility.Collapsed;
                        greenBg.Visibility = Visibility.Collapsed;
                        btnEdit.Visibility = Visibility.Collapsed;
                        btnCheckIn.Visibility = Visibility.Visible;
                        btnExtend.Visibility = Visibility.Visible;
                        btnRelease.Visibility = Visibility.Visible;
                        btnEdit.Visibility = Visibility.Visible;
                        btnExtend.IsEnabled = true;
                        btnRelease.IsEnabled = true;
                        btnEdit.IsEnabled = true;
                        lblMeetingTill.Text = "";                        
                    }
                    else if (meetingRoomEmpty == false && checkedIn == false)
                    {
                        greenBg.Visibility = Visibility.Visible;
                        orangeBg.Visibility = Visibility.Collapsed;
                        redBg.Visibility = Visibility.Collapsed;
                        btnCheckIn.Visibility = Visibility.Collapsed;
                        btnExtend.Visibility = Visibility.Collapsed;
                        btnRelease.Visibility = Visibility.Collapsed;
                        btnEdit.Visibility = Visibility.Collapsed;                        
                        lblStartEndTime.Text = "";
                        lblMeetingRoomStatus.Text = "Available";
                        lblMeetingTill.Text = "";
                        appointments.Remove(item);
                        appointmentControl.Appointments = null;
                        appointmentControl.Appointments = appointments;
                        checkedIn = false;
                        btnExtend.IsEnabled = false;
                        btnRelease.IsEnabled = false;
                        btnEdit.IsEnabled = false;
                    }
                    lblMeetingRoomStatus.Text = nxtMeeting.Subject;
                    lblStartEndTime.Text = nxtMeeting.StartTime.ToShortTimeString() + " - " + nxtMeeting.EndTime.ToShortTimeString();
                }
                else if (isLater = startTime > currentTime && isBetweenMeeting == 0)
                {
                    appointmentId = item.MeetingID;
                    Appointment nxtMeeting = appointments.Where(c => c.MeetingID.Equals(appointmentId)).FirstOrDefault();
                    orangeBg.Visibility = Visibility.Collapsed;
                    redBg.Visibility = Visibility.Collapsed;
                    greenBg.Visibility = Visibility.Visible;
                    lblStartEndTime.Text = "";
                    lblMeetingRoomStatus.Text = "Available";
                    lblMeetingTill.Text = "Till " + nxtMeeting.StartTime.ToShortTimeString();
                    btnCheckIn.Visibility = Visibility.Collapsed;
                    btnExtend.Visibility = Visibility.Collapsed;
                    btnRelease.Visibility = Visibility.Collapsed;
                    btnEdit.Visibility = Visibility.Collapsed;
                    btnExtend.IsEnabled = false;
                    btnRelease.IsEnabled = false;
                    btnEdit.IsEnabled = false;                 
                }
            }
            if (lst.Count == 0)
            {
                orangeBg.Visibility = Visibility.Collapsed;
                redBg.Visibility = Visibility.Collapsed;
                greenBg.Visibility = Visibility.Visible;
                lblStartEndTime.Text = "";
                lblMeetingTill.Text = "";
                lblMeetingRoomStatus.Text = "Available";
                btnCheckIn.Visibility = Visibility.Collapsed;
                btnExtend.Visibility = Visibility.Collapsed;
                btnRelease.Visibility = Visibility.Collapsed;
                btnEdit.Visibility = Visibility.Collapsed;
                checkedIn = false;
                btnExtend.IsEnabled = false;
                btnRelease.IsEnabled = false;
                btnEdit.IsEnabled = false;
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            lblDayMonthDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            LoadHomePage();
        }
                
        private void btnCheckIn_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            ParentPanel.Children.Clear();
            Login lgin = new Login();
            ParentPanel.Children.Add(lgin);
            lgin.myEventLogin += lgin_myEventLogin;
            lgin.loginToCalendar += lgin_loginToCalendar;
        }

        void lgin_loginToCalendar(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
        }

        void lgin_myEventLogin(object sender, EventArgs e)
        {
            checkedIn = true;
            LoadHomePage();
            ParentPanel.Children.Clear();
        }
        
        private void btnExtend_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelExtendDuration.Visibility = Visibility.Visible;
            Appointment extendMeting = appointments.Where(c => c.MeetingID.Equals(currentMeetingID)).FirstOrDefault();
            timePckrEnd.Value = extendMeting.EndTime;
        }
        private void btnSaveExtendDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            DateTime dtEndDate = DateTime.Now;
            Appointment extendMeting = appointments.Where(c => c.MeetingID.Equals(currentMeetingID)).FirstOrDefault();            

            if (timePckrEnd.Value.HasValue)
            {
                string endDate = DateTime.Now.ToShortDateString() + "  " + timePckrEnd.Value.Value.Hour + ":" + timePckrEnd.Value.Value.Minute;
                dtEndDate = Convert.ToDateTime(endDate);                
            }
            extendMeting.EndTime = dtEndDate;
            appointments.Remove(extendMeting);
            appointments.Add(extendMeting);
            appointmentControl.Appointments = null;
            appointmentControl.Appointments = appointments;
            panelExtendDuration.Visibility = Visibility.Collapsed;
        }

        private void btnCancelExtendDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelExtendDuration.Visibility = Visibility.Collapsed;
        }
        
        private void btnRelease_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCheckRelease.Visibility = Visibility.Visible;
        }
        private void btnReleaseCurrentMeeting_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            appointments.Remove(appointments.Where(c => c.MeetingID.Equals(currentMeetingID)).FirstOrDefault());            
            appointmentControl.Appointments = null;
            appointmentControl.Appointments = appointments;
            checkedIn = false;
            ParentPanel.Children.Clear();
            panelCheckRelease.Visibility = Visibility.Collapsed;            
        }
        private void btnReleaseCurrentMeetingClose_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            panelCheckRelease.Visibility = Visibility.Collapsed;
        }

        private void btnEdit_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            Appointment appModify = appointments.Where(c => c.MeetingID.Equals(currentMeetingID)).FirstOrDefault();
            ReSchedule reSchedule = new ReSchedule();
            reSchedule.SelectedAppointment = appModify;
            reSchedule.txtEvent.IsEnabled = true;
            reSchedule.txtAttendees.IsEnabled = true;
            reSchedule.timePicker.IsEnabled = true;
            reSchedule.timePickerEnd.IsEnabled = true;
            reSchedule.cmbAvlMeetingRoom.IsEnabled = true;
            reSchedule.startEditCancelPanel.Visibility = Visibility.Collapsed;
            reSchedule.stkUpdateCancelPanel.Visibility = Visibility.Visible; 

            reSchedule.modifiedAppointment += reSchedule_modifiedAppointment;
            reSchedule.closeReSchedule += reSchedule_closeReSchedule;
            reSchedule.cancelMeeting += reSchedule_cancelMeeting;
            reSchedule.releaseAppointment += reSchedule_releaseAppointment;
            ParentPanel.Children.Add(reSchedule);
            reSchedule.reScheduleToCalendar += reSchedule_reScheduleToCalendar;
        }
    }
}
